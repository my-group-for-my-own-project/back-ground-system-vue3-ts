import request from "../../utils/request";
import type { goodsResData  } from "./type";
enum API {
    GOODS_INFO = '/goods/info',
    // USERINFO_URL = '/user/info'

}

export const reqGoodsInfo = ()=>request.get<string,goodsResData>(API.GOODS_INFO)

// export const reqUserInfo = ()=>request.get<string,userResData,unknown>(API.USERINFO_URL)