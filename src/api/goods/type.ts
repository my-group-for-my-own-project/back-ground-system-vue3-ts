export interface goodsData{
    id?:number,
    name?:string,
    creattime?:string,
    url?:string,
    brand?:string,
    details?:string
}

export interface goodsResData{
    status?:number,
    data?:goodsData
}