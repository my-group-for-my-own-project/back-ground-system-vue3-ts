import request from "../../utils/request";
import type { loginResData,userResData } from "./type";
enum API {
    LOGIN_URL = '/user/login',
    USERINFO_URL = '/user/info'

}

export const reqLogin = (data:string)=>request.post<string,loginResData>(API.LOGIN_URL,data)

export const reqUserInfo = ()=>request.get<string,userResData,unknown>(API.USERINFO_URL)