export interface loginForm{
    name:string,
    password:string
}
export interface loginResData{
    status?:number,
    data?:userInfo
}

interface userInfo{
    userID?:number,
    avatar?:string,
    name?:string,
    nick_name?:string,
    password?:string,
    desc?:string,
    // roles:Array<string>,
    roles?:string[],
    bottons?:Array<string>,
    routes?:Array<string>,
    token?:string,

}
interface user {
    checkUser:userInfo
}
export interface userResData{
    status?:number,
    data:userInfo
}
export interface Result{
    status?:number
    data :userInfo
}
