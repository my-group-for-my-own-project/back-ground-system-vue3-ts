import { createApp } from 'vue'
import App from './App.vue'

// 引入element plus
import ElementPlus from 'element-plus'
// 引入Element plas 的样式
import 'element-plus/dist/index.css'
// 引入element 国际化插件
// 让ts忽略 @ts-ignore
// @ts-ignore
import zhCn from  '@/assets/Internationalization/zh-cn.mjs'
// 引入ElementPlus Icon 
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
//   svg插件配置代码
import 'virtual:svg-icons-register'
//全局 引入icon组件
import SvgIcon from './components/SvgIcon/index.vue'
// 引入路由
import router from './router/index'
// 引入pinia
import pinia from './store'


const app = createApp(App)

// element 组件
app.use(ElementPlus, {
    locale: zhCn, //配置国际化  中文
  })
// 路由
app.use(router)
// 仓库
app.use(pinia)
// 将ElementPlus Icon 全部图标注册
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
// 将icon组件注册为全局组件
app.component('SvgIcon',SvgIcon)
app.mount('#app')