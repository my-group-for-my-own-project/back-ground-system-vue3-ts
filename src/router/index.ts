

import { createRouter, createWebHashHistory } from 'vue-router'
import { routes } from './routes'
import useUser from '../store/modules/user'
import pinia from '../store/index'
 
let router = createRouter({
    history: createWebHashHistory(), 
    //@ts-ignore
    routes: routes,
    // 滚动行为
    scrollBehavior() {
        return {
            left: 0,
            top: 0
        }
    }
});
const userStore = useUser(pinia)
router.beforeEach((to,from,next)=>{
    if(to.meta.isAuth){ //需要 登录状态校验进行校验
        
        // console.log(next)
        let token = userStore.token
        let userName = userStore.userName
        if(token){ //校验token 有则继续校验 是否有用户信息 
                //有用户信息则放行 没有则去获取用户信息，获取不到用户信息证明token 过期/有误需要重新登录
            if(userName){ 
                //用户信息只保存了一个 用户名 没有其他敏感信息，
                // 而且只保存了在了pinia中 刷新就没了 这里不太了解仓库数据用户是否能轻易修改。
                next()
            }else{
                let result = userStore.userinfo()
                result.then(res=>{
                    next()
                }).catch(err=>{
                   next({path:'/login'})
                })
            }
        
       }else{ //没有token直接跳登录页
        // next({path:'/login'})
        console.log('ooo')
        next({path:'/login'})
       }
    }else{  //不需要校验直接放行
        next()
    }
})

router.afterEach((to,from)=>{
    // console.log('后置')
   
    document.title = to.meta.title
})

export default router