export const routes = [
    {
        path: '/login',
        component: () => import('../views/login/index.vue'),
        name: 'Login',
        meta:{
            isAuth:false,
            title: '登录'
        }
    },
    {
        path: '/',
        redirect: '/home',
        component: () => import('../views/main/index.vue'),
        name: 'Main',
        meta:{
            isAuth:true,
            title: '登录'
        },
        children: [
            {
                path: 'home',
                 component: () => import('../views/home/index.vue'),
                name: 'Home',
                meta:{
                    isAuth:true,
                    title: '主页'
                }
            },
            
            {
                path: 'rootUserdata',
                 component: () => import('../views/root/userData.vue'),
                name: 'UserData',
                meta:{
                    isAuth:true,
                    title: '用户管理'
                }
            },
            {
                path: 'rootUserRoot',
                 component: () => import('../views/root/userRoot.vue'),
                name: 'UserRoot',
                meta:{
                    isAuth:true,
                    title: '权限管理'
                }
            },
            {
                path: 'goodsData',
                 component: () => import('../views/goods/goodsData.vue'),
                name: 'goodsData',
                meta:{
                    isAuth:true,
                    title: '商品管理'
                }
            }
        ]
    },
    {
        path: '/screen',
        component: () => import('../views/screen/index.vue'),
        name: 'Screen',
        meta:{
            isAuth:false,
            title: '数据大屏    '
        }
    },
    {
        path: '/404',
        component: () => import('../views/404/index.vue'),
        name: '404',
        meta:{
            isAuth:false,
            title: '404'
        }
    },
    {
        // 任意路由 上面路由都没匹配则 使用任意路由
        path: '/:pathMatch(.*)*',
        redirect: '/404',
        name: 'Any'
    }
]