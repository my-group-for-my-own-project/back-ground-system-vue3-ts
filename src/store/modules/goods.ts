// 创建用户相关的小仓库
import { defineStore } from 'pinia'
// import type { goodsData } from '../../api/goods/type'
import { reqGoodsInfo } from '../../api/goods/index'

let usegoods = defineStore('goods',
    {
        state: () => {
            return{
                
            }           
        },
        actions: { 
            // 获取商品信息
            async goods_info(){
                let result = await reqGoodsInfo()
                if(result.status === 200){
                    return result.data
                }
                return Promise.reject(false)
            }
           
        },
        getters: {

        },
       
    }
)
export default usegoods