/*
 * @Author: GY_books 2567610535@qq.com
 * @Date: 2024-11-05 17:45:00
 * @LastEditTime: 2024-11-07 15:20:45
 * @LastEditors: GY_books 2567610535@qq.com
 * @FilePath: \back-ground-system-vue3-ts\src\store\modules\user.ts
 * @描述: 头部注释配置模板
 */
// 创建用户相关的小仓库
import { defineStore } from 'pinia'
import type { loginForm } from '../../api/user/type'
import { reqLogin, reqUserInfo } from '../../api/user'

let useUser = defineStore('user',
    {
        state: () => {
            return {
                token: localStorage.getItem('TOKEN'),
                userName: ''
            }
        },
        actions: {
            // 用户登录的方法
            async userLogin(data: loginForm) {
                let result = await reqLogin(JSON.stringify(data))
                // console.log(result)
                if (result.status) { // 请求无响应，或者断网没有 status 所以要做下区分
                    if (result.status === 200) {
                        this.token = result.data.token
                        localStorage.setItem('TOKEN', result.data.token)
                        this.userName = result.data.name
                        return '登录成功'
                    }
                    return Promise.reject(result.data)
                }
                return Promise.reject(result)

            },
            // 获取用户信息
            async userinfo() {
                let res = await reqUserInfo()
                if (res.status === 200) {
                    this.userName = res.data.name
                    return res.data
                }
                return Promise.reject(false)
            }
        },
        getters: {

        },

    }
)
export default useUser