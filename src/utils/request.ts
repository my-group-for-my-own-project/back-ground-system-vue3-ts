/*
 * @Author: GY_books 2567610535@qq.com
 * @Date: 2024-11-05 17:45:00
 * @LastEditTime: 2024-11-07 15:26:58
 * @LastEditors: GY_books 2567610535@qq.com
 * @FilePath: \back-ground-system-vue3-ts\src\utils\request.ts
 * @描述: 头部注释配置模板
 */
// axios 封装
import axios from 'axios'

// 创建实例
let request = axios.create({
    baseURL: import.meta.env.VITE_BASE_URL,
    // baseURL:"http://192.168.0.102:8000",
    timeout: 60 * 2 * 1000,
});
request.defaults.headers['Content-Type'] = 'application/json'
// 添加请求和响应拦截器
request.interceptors.request.use(
    config => {
        const token = localStorage.getItem('TOKEN')
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    }
);
// 响应拦截器
request.interceptors.response.use((response) => {
    // 成功回调
    console.log(response)
    return response
}, (error) => {
    let msg: string
    if (error.status === "ERR_NETWORK") {
        msg = '服务器错误,请稍后重试'
    }
    else {
        let status = error.response.status
        switch (status) {
            case 401:
                msg = 'TOKEN过期'
                break
            case 403:
                msg = '无权访问'
                break
            case 404:
                msg = '请求地址有误'
                break
            case "ERR_NETWORK":
                msg = '服务器错误'
                break
            default:
                msg = '网络崩溃'
                break
        }
    }
    return msg

})
export default request