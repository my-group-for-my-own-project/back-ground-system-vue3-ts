/*
 * @Author: GY_books 2567610535@qq.com
 * @Date: 2024-11-05 17:45:00
 * @LastEditTime: 2024-11-07 15:21:32
 * @LastEditors: GY_books 2567610535@qq.com
 * @FilePath: \back-ground-system-vue3-ts\src\utils\upload.ts
 * @描述: 头部注释配置模板
 */
import axios from "axios"
export default class UploadFiles {
    dom: HTMLInputElement = null
    url: string = null
    // data = null
    constructor(dom: HTMLInputElement, url: string) {
        this.dom = dom
        this.url = url
        // this.data = data
    }
    async upload(data) {
        let request = axios.create({
            baseURL: "http://localhost:8000",
            timeout: 5000,
        })
        // request.defaults.headers['Content-Type'] = 'application/json'
        // const files =  this.dom.value?
        const file = this.dom.value['files'][0]
        const formData = new FormData();
        formData.append('file', file);
        formData.append('data', JSON.stringify(data));

        const token = localStorage.getItem('TOKEN');
        let res = await request.post(this.url, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${token}`
            }
        }).then(res => {
            if (res.data.code === 200) {
                return res.data
            }
            return Promise.reject(res.data)
        }).catch(err => {
            console.log(err)
            return Promise.reject(err)
        })
        return res
    }
}


// const axios = require('axios');
// const fileInput = document.querySelector('#fileInput');
// const file = fileInput.files[0];
// const formData = new FormData();
// formData.append('file', file);

// axios.post('/upload', formData, {
// headers: {
//     'Content-Type': 'multipart/form-data'
// }
// }).then(response => {
// console.log('上传成功', response.data);
// }).catch(error => {
// console.error('上传失败', error);
// });