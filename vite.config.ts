
import { defineConfig,UserConfigExport,ConfigEnv, loadEnv} from 'vite'
import {viteMockServe} from 'vite-plugin-mock'
import vue from '@vitejs/plugin-vue'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import path from 'path'


// https://vitejs.dev/config/
export default defineConfig(({command}:ConfigEnv) => {
  // console.log("process",process.env)
  // const env = loadEnv(mode,process.cwd(),"")
  // console.log('env=============',env)

  return {
    plugins: [vue(),
      viteMockServe({
        localEnabled:command === 'serve',//保证开发阶段可以使用mock
        
      }),
    createSvgIconsPlugin({
      iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
      symbolId: 'icon-[dir]-[name]'
    })
    ],
    resolve:{
      alias:{
        "@":path.resolve("./src")
      }
    }
    
  }
})
