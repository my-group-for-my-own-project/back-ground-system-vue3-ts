// vite.config.ts
import { defineConfig } from "file:///E:/Work_web/VUE-PROJECT/back_system_Vue3_TS_Vite/node_modules/.pnpm/vite@4.5.0_sass@1.69.5/node_modules/vite/dist/node/index.js";
import { viteMockServe } from "file:///E:/Work_web/VUE-PROJECT/back_system_Vue3_TS_Vite/node_modules/.pnpm/vite-plugin-mock@2.9.6_mockjs@1.1.0_rollup@2.79.1_vite@4.5.0/node_modules/vite-plugin-mock/dist/index.js";
import vue from "file:///E:/Work_web/VUE-PROJECT/back_system_Vue3_TS_Vite/node_modules/.pnpm/@vitejs+plugin-vue@4.4.0_vite@4.5.0_vue@3.3.7/node_modules/@vitejs/plugin-vue/dist/index.mjs";
import { createSvgIconsPlugin } from "file:///E:/Work_web/VUE-PROJECT/back_system_Vue3_TS_Vite/node_modules/.pnpm/vite-plugin-svg-icons@2.0.1_vite@4.5.0/node_modules/vite-plugin-svg-icons/dist/index.mjs";
import path from "path";
var vite_config_default = defineConfig(({ command }) => {
  return {
    plugins: [
      vue(),
      viteMockServe({
        localEnabled: command === "serve"
        //保证开发阶段可以使用mock
      }),
      createSvgIconsPlugin({
        iconDirs: [path.resolve(process.cwd(), "src/assets/icons")],
        symbolId: "icon-[dir]-[name]"
      })
    ],
    resolve: {
      alias: {
        "@": path.resolve("./src")
      }
    }
  };
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJFOlxcXFxXb3JrX3dlYlxcXFxWVUUtUFJPSkVDVFxcXFxiYWNrX3N5c3RlbV9WdWUzX1RTX1ZpdGVcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZmlsZW5hbWUgPSBcIkU6XFxcXFdvcmtfd2ViXFxcXFZVRS1QUk9KRUNUXFxcXGJhY2tfc3lzdGVtX1Z1ZTNfVFNfVml0ZVxcXFx2aXRlLmNvbmZpZy50c1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9pbXBvcnRfbWV0YV91cmwgPSBcImZpbGU6Ly8vRTovV29ya193ZWIvVlVFLVBST0pFQ1QvYmFja19zeXN0ZW1fVnVlM19UU19WaXRlL3ZpdGUuY29uZmlnLnRzXCI7XG5pbXBvcnQgeyBkZWZpbmVDb25maWcsVXNlckNvbmZpZ0V4cG9ydCxDb25maWdFbnZ9IGZyb20gJ3ZpdGUnXG5pbXBvcnQge3ZpdGVNb2NrU2VydmV9IGZyb20gJ3ZpdGUtcGx1Z2luLW1vY2snXG5pbXBvcnQgdnVlIGZyb20gJ0B2aXRlanMvcGx1Z2luLXZ1ZSdcbmltcG9ydCB7IGNyZWF0ZVN2Z0ljb25zUGx1Z2luIH0gZnJvbSAndml0ZS1wbHVnaW4tc3ZnLWljb25zJ1xuaW1wb3J0IHBhdGggZnJvbSAncGF0aCdcblxuXG4vLyBodHRwczovL3ZpdGVqcy5kZXYvY29uZmlnL1xuZXhwb3J0IGRlZmF1bHQgZGVmaW5lQ29uZmlnKCh7Y29tbWFuZH06Q29uZmlnRW52KSA9PiB7XG4gIHJldHVybiB7XG4gICAgcGx1Z2luczogW3Z1ZSgpLFxuICAgICAgdml0ZU1vY2tTZXJ2ZSh7XG4gICAgICAgIGxvY2FsRW5hYmxlZDpjb21tYW5kID09PSAnc2VydmUnLC8vXHU0RkREXHU4QkMxXHU1RjAwXHU1M0QxXHU5NjM2XHU2QkI1XHU1M0VGXHU0RUU1XHU0RjdGXHU3NTI4bW9ja1xuICAgICAgICBcbiAgICAgIH0pLFxuICAgIGNyZWF0ZVN2Z0ljb25zUGx1Z2luKHtcbiAgICAgIGljb25EaXJzOiBbcGF0aC5yZXNvbHZlKHByb2Nlc3MuY3dkKCksICdzcmMvYXNzZXRzL2ljb25zJyldLFxuICAgICAgc3ltYm9sSWQ6ICdpY29uLVtkaXJdLVtuYW1lXSdcbiAgICB9KVxuICAgIF0sXG4gICAgcmVzb2x2ZTp7XG4gICAgICBhbGlhczp7XG4gICAgICAgIFwiQFwiOnBhdGgucmVzb2x2ZShcIi4vc3JjXCIpXG4gICAgICB9XG4gICAgfVxuICAgIFxuICB9XG59KVxuIl0sCiAgIm1hcHBpbmdzIjogIjtBQUNBLFNBQVMsb0JBQThDO0FBQ3ZELFNBQVEscUJBQW9CO0FBQzVCLE9BQU8sU0FBUztBQUNoQixTQUFTLDRCQUE0QjtBQUNyQyxPQUFPLFVBQVU7QUFJakIsSUFBTyxzQkFBUSxhQUFhLENBQUMsRUFBQyxRQUFPLE1BQWdCO0FBQ25ELFNBQU87QUFBQSxJQUNMLFNBQVM7QUFBQSxNQUFDLElBQUk7QUFBQSxNQUNaLGNBQWM7QUFBQSxRQUNaLGNBQWEsWUFBWTtBQUFBO0FBQUEsTUFFM0IsQ0FBQztBQUFBLE1BQ0gscUJBQXFCO0FBQUEsUUFDbkIsVUFBVSxDQUFDLEtBQUssUUFBUSxRQUFRLElBQUksR0FBRyxrQkFBa0IsQ0FBQztBQUFBLFFBQzFELFVBQVU7QUFBQSxNQUNaLENBQUM7QUFBQSxJQUNEO0FBQUEsSUFDQSxTQUFRO0FBQUEsTUFDTixPQUFNO0FBQUEsUUFDSixLQUFJLEtBQUssUUFBUSxPQUFPO0FBQUEsTUFDMUI7QUFBQSxJQUNGO0FBQUEsRUFFRjtBQUNGLENBQUM7IiwKICAibmFtZXMiOiBbXQp9Cg==
